# README #

Tutorial r�pido de como usar la aplicaci�n:

Vayan al applications.properties. La aplicaci�n lo que hace es leer una lista de "list.source" (yo uso Manabox obviamente, de las listas que se exportan de Collections). El archivo que lee es el de "list.file", que usa el separador "list.separator". Las imagenes resultantes son tiradas al path "binder.destiny", y sus nombres son numericos, pero pueden tener un prefijo "binder.prefix". La cantidad de cartas por ancho y largo de la p�gina las definen en "binder.page.cards.rows" y "binder.page.cards.columns". El tama�o de imagen que se usa de la info de scryfall se define en "retriever.card.size".

Una vez que tengan todas las partes configuradas y los archivos ubicados correctamente, corren BinderCreatorApplication y lo dejan correr. Eventualmente va a terminar o reventar y la aplicaci�n se va a cerrar. Es posible que haya alguna excepci�n durante la ejecuci�n por culpa de alguna carta que no se haya le�do bien. Si pasa, la ejecuci�n continua sin problemas. Al finalizar, va a haber un .txt junto a con la informaci�n de las cartas que no se pudieron leer bien.

Las cartas en las p�ginas mantienen alguna de la informaci�n que le registren de lenguaje, foil y cantidad. No puse m�s, pero se podr�a expandir para poner la condici�n de la carta.

Dise�e la aplicaci�n pensando en el uso de aplicaciones varias de las cuales leer listas y de las cuales ir a buscar la informaci�n, pero honestamente creo que me quedo con estas dos por conveniencia personal. Pero se puede utilizar la parte de lectura de una lista para leer facilmente lo que se tiene guardado y luego transformarlo y/o depositarlo donde se quiera.