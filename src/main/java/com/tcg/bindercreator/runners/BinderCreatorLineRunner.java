package com.tcg.bindercreator.runners;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import com.tcg.bindercreator.components.worker.BinderCreator;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class BinderCreatorLineRunner implements CommandLineRunner {
	
	@Autowired
	private BinderCreator binderCreator;
	
	@Autowired
    private ApplicationContext appContext;
	
	@Override
	public void run(String... args) throws Exception {
		try {
			log.info("Creating binder from command line runner");
			binderCreator.createBinder();
		} finally {
			log.info("Closing the app");
			SpringApplication.exit(appContext);
		}
	}

}
