package com.tcg.bindercreator.components.card.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class CardScryfall {
	
	private String[] multiverse_ids;
	private String name;
	
	private ImageUris image_uris;
	
}
