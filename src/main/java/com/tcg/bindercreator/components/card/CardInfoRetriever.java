package com.tcg.bindercreator.components.card;

import org.springframework.beans.factory.annotation.Value;

import lombok.Getter;

public abstract class CardInfoRetriever {
	protected static final String JPG_FORMAT = "jpg";
	protected static final String PNG_FORMAT = "png";
	
	protected static final String CARD_SMALL = "small";
	protected static final String CARD_NORMAL = "normal";
	protected static final String CARD_LARGE = "large";
	
	@Getter
	@Value("${retriever.card.size}")
	private String cardSize;

	public abstract void fillCardInfo(CardItem cardItem);
	
	public abstract int getCardWidth();
	public abstract int getCardHeight();
	public abstract String getCardImageFormat();
	
}
