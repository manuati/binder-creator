package com.tcg.bindercreator.components.card.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class ImageUris {
	
	private String small;
	private String normal;
	private String large;
}
