package com.tcg.bindercreator.components.card;

import java.util.HashMap;
import java.util.Map.Entry;


public class CardItem {
	
	private HashMap<String, String> fields = new HashMap<String, String>();
	
	public final void addField(Field field, String fieldValue) {
		fields.put(field.name, fieldValue);
	}
	
	@Override
	public String toString() {
		var builder = new StringBuilder();
		builder = builder.append("Card Item:");
		
		for (Entry<String, String> entry : fields.entrySet()) {
			builder = builder.append(", "+entry.getKey()+" : "+entry.getValue());
		}
		
		return builder.toString();
	}
	
	public final String getField(Field field) {
		return fields.get(field.name);
	}
	
	public final boolean hasField(Field field) {
		return fields.containsKey(field.name);
	}
	
	public enum Field {
		AMOUNT("amount"),
		NAME("name"),
		EDITION("edition"), 
		MULTIVERSE_ID("multiverseid"),
		IMAGE_URL("imageurl"),
		FOIL("foil"),
		LANGUAGE("language"),
		SCRYFALL_ID("scryfallid");
		
		private final String name;
		
		private Field(String name) {
			this.name = name;
		}
	}
	
}
