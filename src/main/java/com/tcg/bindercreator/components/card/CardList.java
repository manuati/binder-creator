package com.tcg.bindercreator.components.card;

import java.util.ArrayList;

import lombok.Getter;


public class CardList {
	@Getter
	private ArrayList<CardItem> list = new ArrayList<CardItem>();
	
	public void addItem(CardItem item) {
		if (item == null) return;
		
		if (!list.contains(item)) {
			list.add(item);
		}		
	}
	
	public void emptyList() {
		list = new ArrayList<CardItem>();
	}
	
}
