package com.tcg.bindercreator.components.card;

import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.tcg.bindercreator.components.card.CardItem.Field;
import com.tcg.bindercreator.components.card.dto.CardScryfall;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class MTGScryfallCardInfoRetriever extends CardInfoRetriever {
	
	private static final String SCRYFALL_SEARCH_CARD_ID = "https://api.scryfall.com/cards/{}";
	private static final String SCRYFALL_SEARCH_CARD_MULTIVERSE_ID = "https://api.scryfall.com/cards/multiverse/{}";
	private static final String SCRYFALL_SEARCH_CARD_NAME = "https://api.scryfall.com/cards/named?exact={}";
	

	@Override
	public void fillCardInfo(CardItem cardItem) {
		var restUri = obtainUrlToUse(cardItem);
		var restEntity = new RestTemplate();
		
		log.debug("Fetching info for card {} at uri {}", cardItem.getField(Field.NAME), restUri);
		
		try {
			var scryFallCard = restEntity.getForEntity(restUri, CardScryfall.class).getBody();
			fillCardItemWithItsInfo(cardItem, scryFallCard);
			
			log.info("Card info filled: {}", cardItem);			
		} catch(Exception e) {
			log.warn("Info for card {} could not be retrieved", cardItem.getField(Field.NAME), e);
		}
	}
	
	private String obtainUrlToUse(CardItem cardItem) {
		String id = null;
		String url = null;
		
		if (cardItem.hasField(Field.SCRYFALL_ID)) {
			id = cardItem.getField(Field.SCRYFALL_ID);
			url = SCRYFALL_SEARCH_CARD_ID;
		} else if (cardItem.hasField(Field.MULTIVERSE_ID)) {
			id = cardItem.getField(Field.MULTIVERSE_ID);
			url = SCRYFALL_SEARCH_CARD_MULTIVERSE_ID;
		} else {
			id = cardItem.getField(Field.NAME);
			url = SCRYFALL_SEARCH_CARD_NAME;
		}
		
		return url.replace("{}", id.trim());
	}
	
	private void fillCardItemWithItsInfo(CardItem cardItem, CardScryfall cardInfo) {
		switch (this.getCardSize()) {
			case CardInfoRetriever.CARD_SMALL : cardItem.addField(Field.IMAGE_URL, cardInfo.getImage_uris().getSmall()); break;
			case CardInfoRetriever.CARD_NORMAL : cardItem.addField(Field.IMAGE_URL, cardInfo.getImage_uris().getNormal()); break;
			case CardInfoRetriever.CARD_LARGE : cardItem.addField(Field.IMAGE_URL, cardInfo.getImage_uris().getLarge()); break;
		}
	}

	@Override
	public int getCardWidth() {
		switch (this.getCardSize()) {
			case CardInfoRetriever.CARD_SMALL : return 146;
			case CardInfoRetriever.CARD_LARGE : return 672;
			case CardInfoRetriever.CARD_NORMAL :
			default : return 488;
		}
	}

	@Override
	public int getCardHeight() {
		switch (this.getCardSize()) {
			case CardInfoRetriever.CARD_SMALL : return 204;
			case CardInfoRetriever.CARD_LARGE : return 936;
			case CardInfoRetriever.CARD_NORMAL : 
			default : return 680;
		}
	}

	@Override
	public String getCardImageFormat() {
		return CardInfoRetriever.JPG_FORMAT;
	}
	
}
