package com.tcg.bindercreator.components.worker;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.tcg.bindercreator.components.card.CardInfoRetriever;
import com.tcg.bindercreator.components.card.CardItem;
import com.tcg.bindercreator.components.card.CardItem.Field;
import com.tcg.bindercreator.components.card.CardList;
import com.tcg.bindercreator.components.list.ListReader;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class BinderCreator {
	
	private ListReader reader;
	private CardInfoRetriever cardRetriever;
	
	@Value("${binder.destiny}")
	private String destiny;
	@Value("${binder.prefix}")
	private String prefix;
	@Value("${binder.page.cards.rows}")
	private int rows;
	@Value("${binder.page.cards.columns}")
	private int columns;
	
	private int lastPage = 0;
	
	public BinderCreator(@Autowired ListReader reader, @Autowired CardInfoRetriever cardRetriever) {
		this.reader = reader;
		this.cardRetriever = cardRetriever;
	}

	public void createBinder() throws Exception {
		// Read list file
		CardList list = reader.interpretList();
		log.debug("Cardlist: {}", list.toString());
		log.info("Card list read");
		
		// Fetch the card information from the respective service
		var page = new ArrayList<CardItem>();
		var cardsPerPage = rows * columns;
		var cardsNotAdded = new ArrayList<String>();
		
		log.info("Retrieving card list info");
		for (CardItem card : list.getList()) {
			cardRetriever.fillCardInfo(card);
			
			Thread.sleep(100l);
			
			if (!StringUtils.hasText(card.getField(Field.IMAGE_URL))) {
				log.info("Card not added to the final binder since it does not have an image URL: {}", card);
				cardsNotAdded.add(card.getField(Field.AMOUNT)+" "+card.getField(Field.NAME));
				continue;
			}
			
			page.add(card);
			
			if (page.size() == cardsPerPage) {
				// Create and save the images into a page
				createPageImage(page);
				page.removeAll(page);
			}
		}
		
		if (!page.isEmpty()) {
			createPageImage(page);
		}
		log.info("Finished creating the pages");
		
		if (cardsNotAdded.size() > 0) {
			createListOfCardsNotAdded(cardsNotAdded);
			log.info("List of cards not added finished");
		}		
	}
	
	private void createListOfCardsNotAdded(ArrayList<String> cardsNotAdded) {
		Path finalFile = Paths.get("src", "main", "resources", destiny, "CardsNotAdded.txt");
		
		try {
			Files.deleteIfExists(finalFile);
			log.info("Previous file deleted if existed");
			Files.createFile(finalFile);
			log.info("File for cards not added created");
			FileWriter writer = new FileWriter(finalFile.toFile());
			for(String card : cardsNotAdded) {
				writer.write(card+"\n");
			}
			writer.close();
			log.info("Creation of list of cards not added complete");
		} catch(Throwable t) {
			log.warn("Exception thrown while creating the list of cards not added");
		}
	}
	
	private void createPageImage(ArrayList<CardItem> page) throws IOException {
		if (page.size() == 0) return;
		
		var pageBufferedImage = new BufferedImage(cardRetriever.getCardWidth() * columns, cardRetriever.getCardHeight() * rows, BufferedImage.TYPE_INT_RGB);
		var pageGraphics = pageBufferedImage.createGraphics();
		pageGraphics.setFont(pageGraphics.getFont().deriveFont(cardRetriever.getCardWidth() * 0.2f));
		pageGraphics.setColor(Color.MAGENTA);

		var finalPagePath = getLatestPageFileName();
		
		BufferedImage cardImage;
		int currentRow = 0;
		int currentColumn = 0;
		CardItem card;
		
		try {
			for (int i=0; i < page.size(); i++) {
				if (currentColumn == columns) {
					currentColumn = 0;
					currentRow++;
				}
				
				card = page.get(i);
				cardImage = retrieveCardImage(card);
				pageGraphics.drawImage(cardImage, null, currentColumn * cardRetriever.getCardWidth(), currentRow * cardRetriever.getCardHeight());
				
				drawCardInfo(card, pageGraphics, currentColumn, currentRow);				
								
				currentColumn++;
			}
			
			if (Files.exists(finalPagePath)) {
				Files.delete(finalPagePath);
			}
			
			Files.createDirectories(finalPagePath.getParent());
			Files.createFile(finalPagePath);
			
			ImageIO.write(pageBufferedImage, cardRetriever.getCardImageFormat(), finalPagePath.toFile());
			lastPage++;
		} catch (IOException e) {
			log.warn("Error creating file at path {}", finalPagePath, e);
			throw e;
		} catch (InterruptedException e) {
			log.warn("Interrupted exception while creating file at path {}", finalPagePath, e);
		}
	}
	
	private void drawCardInfo(CardItem card, Graphics2D pageGraphics, int currentColumn, int currentRow) {
		// Draw amount
		if (Integer.parseInt(card.getField(Field.AMOUNT)) > 1) {
			pageGraphics.drawString(
				card.getField(Field.AMOUNT), 
				currentColumn * cardRetriever.getCardWidth() + cardRetriever.getCardWidth() * 0.1f, 
				currentRow * cardRetriever.getCardHeight() +  cardRetriever.getCardHeight() * 0.25f
			);
		}
		
		// Draw foil
		String foil = card.getField(Field.FOIL);
		if (StringUtils.hasText(foil) && "foil".equals(foil)) {
			//Color aux = pageGraphics.getColor();
			//pageGraphics.setColor(Color.MAGENTA);
			pageGraphics.drawString(
				"F", 
				currentColumn * cardRetriever.getCardWidth() + cardRetriever.getCardWidth() * 0.77f, 
				currentRow * cardRetriever.getCardHeight() +  cardRetriever.getCardHeight() * 0.23f
			);
			// pageGraphics.setColor(aux);
		}
		
		// Draw language
		String language = card.getField(Field.LANGUAGE);
		if (StringUtils.hasText(language) && !language.equals("en")) {
			pageGraphics.drawString(
				language, 
				currentColumn * cardRetriever.getCardWidth() + cardRetriever.getCardWidth() * 0.1f, 
				currentRow * cardRetriever.getCardHeight() +  cardRetriever.getCardHeight() * 0.75f
			);
		}
	}

	private Path getLatestPageFileName() {
		return Paths.get("src", "main", "resources", destiny, prefix + lastPage + "." + cardRetriever.getCardImageFormat());
	}
	
	private BufferedImage retrieveCardImage(CardItem card) throws InterruptedException {
		String cardName = "";
		BufferedImage cardImage = null;
		
		try {
			cardName = card.getField(Field.NAME);
			var cardURL = new URL(card.getField(Field.IMAGE_URL));
			cardImage = ImageIO.read(cardURL);
		} catch (MalformedURLException e) {
			log.warn("URL of card '{}' is malformed", cardName, e);
		} catch (IOException e) {
			log.warn("IO Exception for card '{}'", cardName, e);
		} catch (Exception e) {
			log.warn("Exception happened while loading the card image for card {}", cardName, e);
		} finally {
			Thread.sleep(100l);
		}
		
		return cardImage;
	}	
	
}