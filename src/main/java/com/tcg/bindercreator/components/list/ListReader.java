package com.tcg.bindercreator.components.list;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.tcg.bindercreator.components.card.CardList;
import com.tcg.bindercreator.components.card.CardItem;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class ListReader {
	
	private static final String MANABOX = "manabox";
	private static final String DELVER_LENS = "delver_lens";
	
	private static final String COMMA_SEPARATOR = ",";
	
	@Value("${list.separator}")
	private String separator;
	
	@Value("${list.file}")
	private String listFile;
	
	@Value("${list.source}")
	private String source;
	
	private List<String> columnsList;
	
	private SourceColumnsInterpreter columnsInterpreter;
	
	private void configureListReading() {
		switch (source) {
			case MANABOX : {configureReadingForManabox();; break;}
			case DELVER_LENS : {columnsInterpreter = new DelverLensColumnsInterpreter(); break;}
		}
		
		// Most legends have commas in their name. It becomes an issue if the separator is also a comma
		if (COMMA_SEPARATOR.equals(separator)) {
			separator = separator+"(?=(?:[^\\\"]*\\\"[^\\\"]*\\\")*[^\\\"]*$)";			
		}
	}
	
	private void configureReadingForManabox() {
		separator = ",";
		columnsInterpreter = new ManaboxColumnsInterpreter();
	}
	
	
	public CardList interpretList() throws Exception {
		configureListReading();
		
		File file = Paths.get("src", "main", "resources", listFile).toFile();
		
		if (!file.canRead()) {
			log.info("File cannot be read");
			return null;
		}

		BufferedReader reader = null;
		String line;
		var cardList = new CardList();
		try {
			reader = new BufferedReader(new FileReader(file));
			
			line = reader.readLine();
			
			readColumns(line);
			
			while (reader.ready()) {
				cardList.addItem(createItem(reader.readLine()));
			}
		} catch (FileNotFoundException e) {
			log.warn("File not found", e);
		} catch (IOException e) {
			log.warn("IO Exception while reading the file", e);
		} finally {
			if (reader != null) reader.close();
		}
		
		return cardList;
	}
	
	private void readColumns(String rawColumns) {
		columnsList = Arrays.asList(rawColumns.split(separator));
	}
	
	private CardItem createItem(String rawItem) throws Exception {
		if (StringUtils.hasText(rawItem) == false) 
			return null;		
		
		var splitItem = rawItem.split(separator);		
		var finalItem = new CardItem();
		CardItem.Field column;
		log.info("Split item: {}", rawItem);
		for (int i=0; i<splitItem.length; i++) {
			column = columnsInterpreter.getField(columnsList.get(i));
			if (column != null)
				finalItem.addField(column, splitItem[i]);
		}
		
		log.debug("Item created: {}", finalItem);
		return finalItem;
	}
	
}
