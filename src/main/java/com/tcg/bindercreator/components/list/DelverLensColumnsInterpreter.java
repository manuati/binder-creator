package com.tcg.bindercreator.components.list;

import org.springframework.stereotype.Component;

import com.tcg.bindercreator.components.card.CardItem.Field;

//@Component(value = "delver_lens_interpreter")
public class DelverLensColumnsInterpreter extends SourceColumnsInterpreter {

	private static final String QUANTITY = "Quantity";
	private static final String NAME = "Name";
	private static final String MULTIVERSE_ID = "MultiverseID";
	private static final String FOIL = "Foil";
	private static final String LANGUAGE = "Language";
	
	@Override
	public Field getField(String columnName) throws Exception {
		switch(columnName.trim()) {
			case QUANTITY: return Field.AMOUNT;
			case NAME: return Field.NAME;
			case MULTIVERSE_ID: return Field.MULTIVERSE_ID;
			case FOIL: return Field.FOIL;
			case LANGUAGE: return Field.LANGUAGE;
			default: throw new Exception("Unidentified column name: "+columnName);
		}
	}

}
