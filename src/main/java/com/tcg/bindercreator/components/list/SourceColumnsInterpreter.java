package com.tcg.bindercreator.components.list;

import com.tcg.bindercreator.components.card.CardItem.Field;

public abstract class SourceColumnsInterpreter {
	
	public abstract Field getField(String columnName) throws Exception;
	
}
