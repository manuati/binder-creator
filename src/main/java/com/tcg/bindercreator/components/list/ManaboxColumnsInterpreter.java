package com.tcg.bindercreator.components.list;

import org.springframework.stereotype.Component;

import com.tcg.bindercreator.components.card.CardItem.Field;

@Component(value = "manabox_interpreter")
public class ManaboxColumnsInterpreter extends SourceColumnsInterpreter {

	private static final String QUANTITY = "Quantity";
	private static final String NAME = "Name";
	private static final String SCRYFALL_ID = "Scryfall ID";
	private static final String FOIL = "Foil";
	private static final String LANGUAGE = "Language";
	
	@Override
	public Field getField(String columnName) throws Exception {
		switch(columnName.trim()) {
			case QUANTITY: return Field.AMOUNT;
			case NAME: return Field.NAME;
			case SCRYFALL_ID: return Field.SCRYFALL_ID;
			case FOIL: return Field.FOIL;
			case LANGUAGE: return Field.LANGUAGE;
			default: return null;
		}
	}

}
