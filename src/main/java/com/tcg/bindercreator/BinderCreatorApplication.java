package com.tcg.bindercreator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BinderCreatorApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(BinderCreatorApplication.class, args);
	}

	// TODO
	// Opcional: Hacer ambas. Modular la parte de creacion de la carpeta y crear una aplicacion tipo batch, y otra tipo servicio web
	// Opcional: Hacer que las cartas tengan el precio impreso en alguna parte
	// Agregar que ademas se produzca una lista de cartas que no pudieron agregarse a la carpeta final
}
